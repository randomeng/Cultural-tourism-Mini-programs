![输入图片说明](gitee%E5%B0%81%E9%9D%A2.png)

#### 一、官方网址

http://ly.zidingyikeji.com/

商务联系

![输入图片说明](%E5%95%86%E5%8A%A1%E8%81%94%E7%B3%BB1.png)


#### 二、亮点功能

1．智能语音导游，深度解读景区历史文化；

2．游玩路线推荐，贴心规划；

3．在线预约景区服务，无需排队等待；

4．景区活动展示，精彩瞬间不错过；

5．未完待续....

#### 三、版本功能

![输入图片说明](%E6%99%BA%E6%85%A7%E6%96%87%E6%97%85%E5%B0%8F%E7%A8%8B%E5%BA%8F07-15.png)

#### 四、操作截图

1.0 旅客通

![输入图片说明](%E6%99%BA%E6%85%A7%E6%96%87%E6%97%85%E5%B0%8F%E7%A8%8B%E5%BA%8Flogo.png)

小程序部分页面展示

![输入图片说明](%E6%99%BA%E6%85%A7%E6%96%87%E6%97%85%E5%B0%8F%E7%A8%8B%E5%BA%8F%E9%83%A8%E5%88%86%E5%9B%BE%E7%89%8707-15.png)

#### 五、更多功能

需了解更多功能，请访问：https://gitee.com/randomeng/smart-cultural-tourism/blob/master/README.md



